#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <iomanip>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>
#include <mutex>
#include <syslog.h>
#include <future>

#define BUF_LEN 26

struct js_event {
  unsigned int time;      /* event timestamp in milliseconds */
  short value;            /* value */
  unsigned char type;     /* event type */
  unsigned char number;   /* axis/button number */
};

uint16_t chan[8] = {1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500};
unsigned char dsm_map[8] = { 1, 2, 0, 3, 4, 5, 6, 7 }; // JS uses AETR ordering, DSMX uses TAER ordering
uint16_t sequence = 0;
uint64_t timestamp = 0;
uint8_t buf[BUF_LEN];
std::mutex chanMutex;

uint16_t map_short_to_rc(short val)
{
  static const int js_min = std::numeric_limits<short>::min();
  static const int js_max = std::numeric_limits<short>::max();
  static const int rc_min = 1000;
  static const int rc_max = 2000;
  static const double ratio = ((double) (rc_max - rc_min) / (js_max - js_min));
  return (uint16_t) (rc_min + (ratio * (val - js_min)));
}

void thread_readjs(std::string jsdev)
{
  int fd = open(jsdev.c_str(), O_RDONLY);
  if (fd < 0) {
    syslog(LOG_ERR, "Can't open joystick device");
    exit(-1);
  }
  syslog(LOG_INFO, "Opened joystick device at %s", jsdev.c_str());
  struct js_event e;
  while (true) {
    int ret = read(fd, &e, sizeof(e));
    if (ret == sizeof(e)) {
      // successful joy read
      const unsigned char axis = dsm_map[e.number];
      if (axis >= 0 && axis < 8) {
        std::lock_guard<std::mutex> lk(chanMutex);
        chan[axis] = map_short_to_rc(e.value);
        timestamp = e.time;
      }
    }
  }
}

void thread_udp(std::string dest_ip, uint16_t dest_port)
{
  auto next_print = std::chrono::system_clock::now();

  int sockfd;
  ssize_t n;
  socklen_t serverlen;
  struct sockaddr_in serveraddr;
  struct hostent *server;

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) {
    syslog(LOG_ERR, "Couldn't create UDP socket");
    return;
  }

  server = gethostbyname(dest_ip.c_str());
  if (server == NULL) {
    syslog(LOG_ERR, "Couldn't find host %s to send UDP to", dest_ip.c_str());
    return;
  }

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  bcopy((char *) server->h_addr,
        (char *) &serveraddr.sin_addr.s_addr, server->h_length);
  serveraddr.sin_port = htons(dest_port);

  serverlen = (socklen_t) sizeof(serveraddr);

  while (true) {

    if (timestamp != 0) {
      {
        std::lock_guard<std::mutex> lk(chanMutex);
        memset(buf, 0, sizeof(buf));
        memcpy(buf, &timestamp, 8);
        memcpy(&buf[8], &sequence, 2);
        memcpy(&buf[10], chan, 16);
      }

      sendto(sockfd, buf, BUF_LEN, 0, (struct sockaddr *) &serveraddr, serverlen);

      auto now = std::chrono::system_clock::now();
      if (now > next_print) {
        syslog(LOG_DEBUG, "{ts,seq,ch1...ch8}: %ld %d %d %d %d %d %d %d %d %d",
               (unsigned long)timestamp, (unsigned int) sequence, (unsigned int) chan[0], (unsigned int) chan[1],
               (unsigned int) chan[2], (unsigned int) chan[3], (unsigned int) chan[4],
               (unsigned int) chan[5], (unsigned int) chan[6], (unsigned int) chan[7]);

        next_print = now + std::chrono::seconds(5);
      }

      sequence += 1;
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
  }
}

template<typename T>
bool not_ready(std::future<T> &t)
{
  return t.wait_for(std::chrono::seconds(0)) != std::future_status::ready;
}

int main(int argc, char *argv[])
{
  if (argc != 4) {
    std::cerr << "usage: js2rc [joystick dev] [dest ip] [dest port]" << std::endl;
    exit(-1);
  }

  syslog(LOG_INFO, "Joystick: %s", argv[1]);
  syslog(LOG_INFO, "Destination IP: %s", argv[2]);
  syslog(LOG_INFO, "Destination Port: %s", argv[3]);

  std::string jsdev = argv[1];
  std::string destip = argv[2];
  auto destport = (uint16_t) std::atoi(argv[3]);

  std::future<void> jsFuture = std::async(std::launch::async, thread_readjs, jsdev);
  std::future<void> udpFuture = std::async(std::launch::async, thread_udp, destip, destport);

  while (not_ready(jsFuture) && not_ready(udpFuture)) {
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  }

  return 0;
}