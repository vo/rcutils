#include <Arduino.h>
#include <adafruit_feather.h>
#include "Arduhdlc.h"

#define WLAN_SSID             "myssid"
#define WLAN_PASS             "mypassword"

int led_pin = PA15;

// udp stuff
IPAddress server_ip(10, 0, 1, 115);
const uint16_t port = 5005;
AdafruitUDP udp;
const uint16_t local_port = 5005;

// hdlc stuff
#define MAX_HDLC_FRAME_LENGTH 32
void send_character(uint8_t data);
void hdlc_frame_handler(const uint8_t *data, uint16_t length);
Arduhdlc hdlc(&send_character, &hdlc_frame_handler, MAX_HDLC_FRAME_LENGTH);

// rc stuff
uint16_t seq = 0;
unsigned char dsm_map[8] = { 1, 2, 0, 3, 4, 5, 6, 7 }; // JS uses AETR ordering, DSMX uses TAER ordering

bool connectAP(void)
{
  Serial.print("Please wait while connecting to: '" WLAN_SSID "' ... ");
  if ( Feather.connect(WLAN_SSID, WLAN_PASS) )
  {
//    Serial.println("Connected!");
  }
  else
  {
//    Serial.printf("Failed! %s (%d)", Feather.errstr(), Feather.errnum());
//    Serial.println();
  }
//  Serial.println();
  return Feather.connected();
}

void send_character(uint8_t data) {
    Serial1.print((char)data);
}

void hdlc_frame_handler(const uint8_t *data, uint16_t length) {
  if(length == 24) {
    uint64_t timestamp;
    uint16_t chan[8];

    // unpack
    memcpy(&timestamp, data, 8);
    memcpy(&chan, &data[8], 18);

    // remap channels
    uint16_t chan_remap[8];
    for(int i = 0; i < 8; ++i) {
      chan_remap[dsm_map[i]] = chan[i];
    }

    // repack
    uint8_t pkt[26];
    memset(pkt, 0, sizeof(pkt));
    memcpy(pkt, &timestamp, 8);
    memcpy(&pkt[8], &seq, 2);
    memcpy(&pkt[10], chan_remap, 16);
    
    udp.beginPacket(server_ip, port);
    udp.write(pkt, 26);
    udp.endPacket();

//    Serial.printf("%ld", timestamp);
//    for(int i = 0; i < 8; ++i) {
//      Serial.printf(" %d", chan_remap[i]);
//    }
//    Serial.println();

    seq++;
  }
}

void setup()
{
  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);
//  Serial.begin (115200);
  Serial1.begin(115200);
//  while (!Serial) {
//    delay(1);
//  }
//  Serial.println("Connecting to wifi...");
  while(!connectAP()) {
    delay(500);
  }
  digitalWrite(led_pin, HIGH);
  udp.begin(local_port);
}

void loop()
{
  while (Serial1.available()) {
    char inChar = (char)Serial1.read();
    hdlc.charReceiver(inChar);
  }
}
