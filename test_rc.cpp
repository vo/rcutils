#include <syslog.h>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <ctime>
#include <termios.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <csignal>

#include "ini/cpp/INIReader.h"
#include "dsm/dsm.h"

int openSerial(const std::string &deviceName)
{
  struct termios options;

  int serial_fd = open(deviceName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

  if (serial_fd < 0) {
    fprintf(stderr, "serial: can't open %s", deviceName.c_str());
    return serial_fd;
  }

  // Configure port for 8N1 transmission
  tcgetattr(serial_fd, &options);
  cfsetospeed(&options, B115200);

  options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
  options.c_oflag &= ~(OCRNL | ONLCR | ONLRET | ONOCR | OFILL | OPOST);
  options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
  options.c_cflag &= ~(CSIZE | PARENB);
  options.c_cflag |= CS8;

  tcsetattr(serial_fd, TCSANOW, &options);

  return serial_fd;
}

int main(int argc, char * argv[])
{

  PwmToDsm *pwmToDsmThrottle = NULL;
  PwmToDsm *pwmToDsmOther = NULL;

  INIReader reader("/etc/uav.conf");
  if (reader.ParseError() < 0) {
    std::cout << "Can't load /etc/uav.conf\n";
    return -1;
  }
  std::string serialPortName = reader.Get("rc", "rcDsmDev", "/dev/ttyAMA0");
  int pwmInMinThrottle = (int)reader.GetInteger("rc", "pwmInMinThrottle", 1000);
  int pwmInMaxThrottle = (int)reader.GetInteger("rc", "pwmInMaxThrottle", 2000);
  int pwmOutMinThrottle = (int)reader.GetInteger("rc", "pwmOutMinThrottle", 1000);
  int pwmOutMaxThrottle = (int)reader.GetInteger("rc", "pwmOutMaxThrottle", 1900);
  int pwmInMinOther = (int)reader.GetInteger("rc", "pwmInMinOther", 1000);
  int pwmInMaxOther = (int)reader.GetInteger("rc", "pwmInMaxOther", 2000);
  int pwmOutMinOther = (int)reader.GetInteger("rc", "pwmOutMinOther", 1000);
  int pwmOutMaxOther = (int)reader.GetInteger("rc", "pwmOutMaxOther", 2000);


  // create the mappers
  pwmToDsmThrottle = new (std::nothrow)PwmToDsm(pwmInMinThrottle, pwmInMaxThrottle, pwmOutMinThrottle, pwmOutMaxThrottle);
  if (pwmToDsmThrottle == NULL) {
    std::cerr << "Could not create pwmToDsmThrottle" << std::endl;
    return -1;
  }
  pwmToDsmOther = new (std::nothrow)PwmToDsm(pwmInMinOther, pwmInMaxOther, pwmOutMinOther, pwmOutMaxOther);
  if (pwmToDsmOther == NULL) {
    std::cerr << "Could not create pwmToDsmOther" << std::endl;
    return -1;
  }

  // open serial port
  int serialFd = openSerial(serialPortName);
  if(serialFd < 0) {
    std::cerr << "Could not open serial port " << serialPortName << std::endl;
    return serialFd;
  }

  uint16_t channel[8];

  while(true) {
    // todo: update the channels

    // generate dsm string
    char dsmStr[32];
    memset(dsmStr, 0, sizeof(dsmStr));
    int num_bytes = encode_DSM(dsmStr, &channel[0], 8, pwmToDsmThrottle, pwmToDsmOther);

    // write to the DSM port
    if (write(serialFd, dsmStr, num_bytes) != num_bytes) {
      syslog(LOG_ERR, "serial: port write error");
    }
  }

  return 0;
}