#include <PulsePosition.h>
#include "Arduhdlc.h"

PulsePositionInput ppmIn;

#define MAX_HDLC_FRAME_LENGTH 32

void send_character(uint8_t data);
void hdlc_frame_handler(const uint8_t *data, uint16_t length);

Arduhdlc hdlc(&send_character, &hdlc_frame_handler, MAX_HDLC_FRAME_LENGTH);

void setup() {
//  Serial.begin(115200);
  Serial1.begin(115200);
  ppmIn.begin(6);
  pinMode(13, OUTPUT);   
  digitalWrite(13, HIGH);
}

void loop() {
  int num = ppmIn.available();
  
  if(num == 8) {
    char buf[24];
    uint64_t ts = micros();  
    uint16_t chan[8] = {1500,1500,1500,1500,1500,1500,1500,1500};
//    Serial.printf("%ld\t", ts);
    
    // get ppm data
    for(int i = 1; i <= 8; ++i) {
      uint16_t val = (uint16_t)ppmIn.read(i);
      chan[i-1] = val;
//      Serial.print(val);
//      Serial.print('\t');  
    }
//    Serial.println();
    
    // insert into buffer
    memset(buf, 0, sizeof(buf));
    memcpy(buf, &ts, 8);
    memcpy(&buf[8], chan, 16);
    
    // output hdlc
    hdlc.frameDecode(buf, 24);
  }
}

void send_character(uint8_t data) {
    Serial1.print((char)data);
}

void hdlc_frame_handler(const uint8_t *data, uint16_t length) {
    // for receiving, not used
}

void serialEvent() {
    while (Serial1.available()) {
        char inChar = (char)Serial1.read();
        hdlc.charReceiver(inChar);
    }
}
