js2rc is a linux program that reads from a USB joystick and outputs UDP packets containing timestamp, sequence number, and RC channels. The Taranis controller for example, can be used as a USB joystick input for this program.

readppm-teensy reads PPM joystick values (e.g. from an RC receiver, or from the PPM output on a RC transmitter) and outputs them via UART using HDLC format.
It is designed for a Teensy 3.2

readhdlc-wiced reads HDLC joystick values from a serial port and outputs them via UDP using the pixrc protocol.
It is designed for an Adafruit Feather WICED Wifi.

test_rc is just a test program that sends UDP joystick packets.
